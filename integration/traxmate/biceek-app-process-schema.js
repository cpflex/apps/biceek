"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Run = void 0;
async function Run(o, _device) {
  // The raw data received from the device. Should not be edited.
  const orgData = o.data.original.data;

  let nid = "";
  if (typeof orgData.nid !== "undefined") {
    nid = orgData.nid;
  }

  switch (nid) {
    //-------------------------------------
    // Operation Mode Status.
    //-------------------------------------
    case "App.Opmode": {
      if (orgData.svals[0] === "App.Opmode.Normal") {
        _device.signals.set("Mode", "Mode Status Normal");
        _device.signals.set("Status", "Device operating in normal mode.");
      } else if (orgData.svals[0] === "App.Opmode.Recovery") {
        _device.signals.set("Mode", "Mode Status Recovery");
        _device.signals.set("Status", "Device is operating in recovery mode.");
      }
      break;
    }

    //-------------------------------------
    // Motion Notification.
    //-------------------------------------
    case "App.Motion": {
      if (orgData.svals[0] === "App.Motion.Moving") {
        _device.signals.set("Mode", "Active Motion Event");
        _device.signals.set("Status", "Device is in motion.");
      } else if (orgData.svals[0] === "App.Motion.Stopped") {
        _device.signals.set("Mode", "Stop Motion Event");
        _device.signals.set("Status", "Device is stopped (parked)");
      }
      break;
    }
  }
}
exports.Run = Run;
