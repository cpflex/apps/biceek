@ECHO OFF
if [%1]==[] goto usage

set specfile="cphub_tag_spec.json"
set tag=%1
ECHO Pushing Biceek Application version %1
cphub push -s %specfile% cpflexapp "./biceek.bin" /bobcat/apps/biceek:%tag%

goto :eof
:usage
@echo Usage: %0 ^<tag (e.g. v1.0.2.3)^>
exit /B 1
