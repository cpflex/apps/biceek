/**
 *  Name:  Controller.i
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2021 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */

 #include "telemetry.i"

 /**
 * @brief Defines the states of the mode controller.
 */
 const ControllerState: {
	STATE_IDLE = 0,				//Controller is in an idle state waiting for a motion event.
	STATE_ENTER_MOTION,			//Controller is determining is device is in continuous motion.
	STATE_IN_MOTION,			//Controller is monitoring a device in continuous motion, waiting for no motion signal.
	STATE_EXIT_MOTION,			//Controller is determining if no motion is continous for some period of time.
	STATE_DISABLED,				//Controller is in a disabled state typically when power saving is enabled.
	STATE_RECOVERY				//Controller is in a recovery mode periodically reporting location.	
 };
 

/**
* @brief Initializes the controller 
*/
forward ControllerInit( );

/**
* @brief Enables / Disables recovery mode.
*/
forward ControllerRecoveryMode( bool: bEnable);

/**
* @brief Procsses downlink messages applicable to battery module.
* @param seqOut The output sequence to send any uplinks.
* @param tmsg The message type.
* @param id The message id.
* @param msg The message.
* @returns Returns the count of messages posted to seqOut.
*/
forward int: ControllerDownlink( Sequence: seqOut, MsgType: tmsg, id, Message: msg);


/**
* @brief Notifies controller when power savings changes 
*/
forward ControllerPowerSavingUpdate( bool: bEnable);

/**
* @brief Notifies the controller with the telemtry network status changes.
*/
forward ControllerTelemetryStatusUpdate( TelemState: stateTelem, datarate, packetsize);