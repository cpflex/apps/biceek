/**
*  Name:  ConfigApp.i
*
*  This module is the proprietary property of Codepoint Technologies, Inc.
*  Copyright (C) 2021 Codepoint Technologies, Inc.
*  All Rights Reserved
*
* Description:
*  Configuration constants for network link Checks.
**/

#include "Controller.i"
#include "motion.i"

#ifdef __TEST__
	const ControllerState: CONTROLLER_DEFAULT_STATE		= STATE_IDLE;

	//Seconds of innactivity before exiting Motion State
	//210907 mbm Set to 180 seconds for test build
	const CONTROLLER_INNACTIVITY_INTERVAL = 180; 
#else
	const ControllerState: CONTROLLER_DEFAULT_STATE		= STATE_IDLE;

	//Seconds of innactivity before exiting Motion State
	const CONTROLLER_INNACTIVITY_INTERVAL = 600;
#endif

const MotionResolution: CONTROLLER_IDLE_RESOLUTION	= MRES_LOW_POWER;
const MotionSampling: CONTROLLER_IDLE_RATE			= MRATE_1HZ;
const CONTROLLER_IDLE_THRESHOLD     = 100;

const MotionResolution: CONTROLLER_MOTION_RESOLUTION	= MRES_LOW_POWER;
const MotionSampling: CONTROLLER_MOTION_RATE		= MRATE_10HZ;
const CONTROLLER_MOTION_THRESHOLD   = 500;
const CONTROLLER_CHECK_INTERVAL = 300;  //Check network for downlink messages.

const MOTION_SAMPLING_PERIOD		= 10; //Motion sampling period in seconds must be less than intervals.

