/**
*  Name:  ConfigSimpleTracking.i
*
*  This module is the proprietary property of Codepoint Technologies, Inc.
*  Copyright (C) 2021 Codepoint Technologies, Inc.
*  All Rights Reserved
*
* Description:
*  Configuration constants for network link Checks.
**/

#include "ui.i"
#include "telemetry.i"
#include <OcmNidDefinitions.i>


//NID Command Map, by default these map to ped_tracker protocol spec.
//If your protocol is different, update the NID mappings.
const _: {
	NM_INTERVAL 	= _: NID_TrackConfigNomintvl,
	NM_CMD_CONFIG	= _: NID_TrackConfig,
	NM_CMD_TRACKING	= _: NID_TrackMode,
	NM_ACQUIRE		= _: NID_TrackConfigAcquire,
	NM_ENABLE		= _: NID_TrackModeEnabled,
	NM_DISABLE		= _: NID_TrackModeDisabled		
};

// Acquire positioning data indicator
const 			TRACKING_IND_LED_ACTIVATE    = LED1;
const 			TRACKING_IND_LED_DEACTIVATE  = LED2;
const 			TRACKING_IND_COUNT = 4;
const  bool:    TRACKING_IND_ENABLE = false;		//Tracking activation / deactivation indicator is enabled.
const  bool:	ACQUIRE_IND_ENABLE	= false;	//Acquire indicator disabled for this application.
const			ACQUIRE_IND_LED		= LED1;		//LED to use for indication.
const			ACQUIRE_IND_COUNT	= 2;		//Number of cycles to blink.
const			ACQUIRE_IND_ON		= 466;		//Blink on-time in milliseconds.
const			ACQUIRE_IND_OFF		= 133;		//Blink off-time in milliseconds.

// Persisted variable default values.
//These values define the initial value for persisted configurations.
//These are set when the application is first run after installation.
const			DEFAULT_INTERVAL	= 3;		//Default Tracking interval in minutes.
const  bool:	DEFAULT_ACTIVE		= false;	//Default Tracking state is inactive.

//Default is to confirm and archive all acquisition messages during 
//normal mode.
const  TelemPostFlags: DEFAULT_FLAGS_POSTACQUIRE = TPF_CONFIRM_ARCHIVE;


//Comment these out to use the Location Config Module.
const PositioningMode: POSITIONING_MODE = _: PM_coarse;  //PM_default;
const PositioningTechnologies: POSITIONING_TECH = _: PT_wifi;    //PT_wifi_ble;
