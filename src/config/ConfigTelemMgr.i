/**
*  Name:  ConfigSimpleTracking.i
*
*  This module is the proprietary property of Codepoint Technologies, Inc.
*  Copyright (C) 2021 Codepoint Technologies, Inc.
*  All Rights Reserved
*
* Description:
*  Configuration constants for network link Checks.
**/

#include "ui.i"

// Telemetry Status Indications.
const  INDICATE_LED_NORMAL   = LED1;
const  INDICATE_LED_CRITICAL = LED2;
const  INDICATE_DISABLED_ON   = 500;
const  INDICATE_DISABLED_OFF  = 250;
const  INDICATE_UNAVAILABLE_ON   = 750;
const  INDICATE_UNAVAILABLE_OFF  = 250;
const  INDICATE_RSS_ON      = 500;		
const  INDICATE_RSS_OFF     = 250;

const  DEFAULT_POLLING = 360;  // Default polling interval is every 6 hours.

//NID Command Map, by default these map to system protocol spec.
//If your protocol is different, update the NID mappings.
const NIDTELEMMAP: {
	NM_TELEM_SETTINGS  = _: NID_SystemConfig, // Telemtry Settings..
	NM_TELEM_POLL_INTVL = _: NID_SystemConfigPollintvl, //Downlink / Health Polling Interval.
};
