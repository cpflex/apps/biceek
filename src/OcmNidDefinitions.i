/*******************************************************************************
*                !!! OCM CODE GENERATED FILE -- DO NOT EDIT !!!
*
* Biceek Application Protocol
*  nid:        Biceek
*  uuid:       8d9c9b19-feb0-49c2-bc21-77e931ba7da1
*  ver:        1.0.0.0
*  date:       2023-10-12T23:09:36.668Z
*  product: Cora Tracking CT10XX
* 
*  Copyright 2023 - Codepoint Technologies, Inc. All Rights Reserved.
* 
*******************************************************************************/
const OcmNidDefinitions: {
	NID_LocConfigPmode = 1,
	NID_SystemLogAll = 2,
	NID_LocConfigPmodeDefault = 3,
	NID_TrackConfig = 4,
	NID_SystemLogDisable = 5,
	NID_PowerBattery = 6,
	NID_LocConfigPtech = 7,
	NID_SystemReset = 8,
	NID_PowerChargerCritical = 9,
	NID_TrackConfigNomintvl = 10,
	NID_AppMotion = 11,
	NID_SystemConfigPollintvl = 12,
	NID_SystemLog = 13,
	NID_AppOpmode = 14,
	NID_TrackMode = 15,
	NID_SystemLogAlert = 16,
	NID_LocConfigPmodePrecise = 17,
	NID_LocConfigPtechBle = 18,
	NID_LocConfigPtechWifi = 19,
	NID_TrackModeActive = 20,
	NID_LocConfig = 21,
	NID_LocConfigPmodeMedium = 22,
	NID_AppOpmodeNormal = 23,
	NID_PowerCharger = 24,
	NID_LocConfigPmodeCoarse = 25,
	NID_TrackModeDisabled = 26,
	NID_TrackConfigEmrintvl = 27,
	NID_AppMotionMoving = 28,
	NID_SystemErase = 29,
	NID_LocConfigPtechWifible = 30,
	NID_AppMotionStopped = 31,
	NID_LocConfigPtechAutomatic = 32,
	NID_AppOpmodeRecovery = 33,
	NID_PowerChargerCharging = 34,
	NID_SystemLogInfo = 35,
	NID_TrackConfigAcquire = 36,
	NID_TrackConfigInactivity = 37,
	NID_SystemLogDetail = 38,
	NID_PowerChargerCharged = 39,
	NID_SystemConfig = 40,
	NID_PowerChargerDischarging = 41,
	NID_TrackModeEnabled = 42
};
