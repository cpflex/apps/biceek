/**
 *  Name:  app.p
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2023 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */

 #include "app.i"
 #include "version.i" //Compile time generated file.

//System includes.
#include "cpflexver.i"

//Modules
#include "battery.i"
#include "TelemMgr.i"
#include "SysMgr.i"
#include "LocationConfig.i"
#include "Controller.i"
#include "StatefulEvent.i"

//Include Controller configuration.
#include "config/ConfigApp.i"

/*******************************************************
* Forward Declarations.
*******************************************************/

/*******************************************************
* Constants and static variables.
*******************************************************/

/*******************************************************
* System events.
*******************************************************/

@SysInit()
{
	Log(   "************ Traxmate Bike App ****************",	MC_info, MP_med, LO_default);
	LogFmt("** Version:      %s",								MC_info, MP_med, LO_default, __VERSION__);
	LogFmt("** CP-Flex Ver:  %s",								MC_info, MP_med, LO_default, CPFLEX_VERSION);
	LogFmt("** Build Date:   %s %s",							MC_info, MP_med, LO_default, __DATE__, __TIME__);
	Log(   "***********************************************",	MC_info, MP_med, LO_default);


#ifdef __SYSLOG__
//	//Test mode we automatically enable the System Log.
	SysSetLogFilters( MsgCategory: 0xFF, MsgCategory: 0xFF);
#endif 

	loccfg_Init();
	battery_Init();
	TelemMgr_Init();
	SysmgrInit();	
	StatefulEventInit();
	ControllerInit();
}

/*******************************************************
* Handle UI Events.
*******************************************************/
@UiButtonEvent( idButton, ButtonPress: press, ctClicks)
{
	TraceDebugFmt( "Button: id=%d, press=%d, clicks=%d", _:idButton, _:press, _:ctClicks);
	switch(press) {
		case BP_short: 		{	battery_IndicateChargeLevel(); }
		case BP_double:		{   TelemMgr_IndicateNetworkStatus(); }

#ifdef __TEST__
		case BP_multi:   {  
			ControllerRecoveryMode( ctClicks > 4);
		}
#endif
	}
}

#pragma warning disable 203

/**
* @brief  Application Defined callback handler that indicates entering power savings.
* @remarks Required implementation to catch enter power saving events.
*/
app_EnterPowerSaving(pctCharged)
{
	// Tracking is deactivated to preserve battery.	
	TraceDebug("Enter Power Save Mode");
	ControllerPowerSavingUpdate(false);
}

/**
* @brief  Application Defined callback handler that indicates entering power savings.
* @remarks Required implementation to catch exit power saving events.
*/
app_ExitPowerSaving(pctCharged)
{
	TraceDebug("Exit Power Save Mode");
	ControllerPowerSavingUpdate(true);
}

/***
* @brief Callback event handler implemented by the application to receive
*   notification of state changes and signals enabled by the state definitions.
* @param id The callback event identifier.
* @param epoch The time of the event.
* @param ctMeas The number of measurements in the event.
*/
app_TrkCallbackEvent(  
	idTcbe,
    epoch,
    ctMeas
){
	//Nothing todo.
}


/***
* @brief Callback event handler implemented by the application to receive
*   notification of event state changes.
* @param idEvent The stateful event identifier.
* @param newState  The new state identifier.
*/
app_StatefulEventCallback(  idEvent, StatefulEventState: newState)
{
	//Notihing todo.
}



#pragma warning enable 203


/*******************************************************
* Communication Implementation
*******************************************************/
/**
* @brief Event hander receives notifications of telemtry system changes.
* @remarks Use this event to trigger other actions when telemetry is available or unavailable.
* @param stateTelem The updated change of telemtry state.
* @param datarate  The datarate in data rate units (e.g. dr0 -dr12).
* @param packetsize The uplink/downlink packetsize in bytes.  -1 is unknown.
*/
@TelemStatusEvent( TelemState: stateTelem, datarate, packetsize)
{
	//Forward notification to the controller.
	ControllerTelemetryStatusUpdate( stateTelem, datarate, packetsize);
}

/**
* @brief Received data sequence handler.
* @remarks.  Processes received sequences and reports any requested. data.
*/
@TelemRecvSeq( Sequence: seqIn, ctMsgs )
{
	//Process the received sequence.
	new Message: msg;
	new MsgType: tmsg;
	new tcode;
	new id;
	new MsgPriority: priority;
	new MsgCategory: category;
	new ResultCode: rc;
	new int: ct= int: 0;
	new Sequence:seqOut;

	//Allocate a sequence to send messages.
	//Create a sequence to hold all potential commands.
	rc = TelemSendSeq_Begin( seqOut, 150); 
	if( rc != RC_success)
	{
		TraceError("Could not allocate sequence buffer, ");
	}
	
	//Process each of the received messages.
	while( rc == RC_success 
		&& TelemRecv_NextMsg( seqIn, msg, tmsg, tcode, id, priority, category) 
			== RC_success)
	{		
	 	//Process using Module Handlers.
		ct += ControllerDownlink( seqOut, tmsg, id, msg);
		ct += battery_Downlink( seqOut, tmsg, id, msg);
		ct += SysmgrDownlink( seqOut, tmsg, id, msg);
		ct += TelemMgr_Downlink( seqOut, tmsg, id, msg);
		ct += loccfg_downlink( seqOut, tmsg, id, msg);
		ct += StatefulEventDownlink( seqOut, tmsg, id, msg);
	}

	//Done with the input sequence.
	TelemRecv_SeqEnd( seqIn);

	//Either send or discard output sequence 
	//send only if there are messages to send and we were successful in creating them.
	TelemSendSeq_End( seqOut, rc != RC_success || ct == int:0, TPF_CONFIRM);
	if( rc != RC_success)
		TraceError("Cannot send sequence an error occurred while assembling.");
}


