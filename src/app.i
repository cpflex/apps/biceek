/**
 *  Name:  app.i
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2023 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */
 
#include "ui.i"
#include "timer.i"
#include "log.i"
#include "telemetry.i"
#include "NvmRecTools.i"
#include "OcmNidDefinitions.i"
 
/**
* @brief Records defined to manage persisted application data.
*/
const nvmrec: {
	NVREC_CONTROLLER_MODE	   = 0x1, // Controller Mode.
	NVREC_CONTROLLER_PREVMODE = 0x2  // Controller Previous Mode.
}

const {
	EVENT_MOTION=0
}

/**
* @brief  Application Defined callback handler that indicates entering power savings.
* @remarks Required implementation to catch enter power saving events.
*/
forward app_EnterPowerSaving(pctCharged);

/**
* @brief  Application Defined callback handler that indicates entering power savings.
* @remarks Required implementation to catch exit power saving events.
*/
forward app_ExitPowerSaving(pctCharged);

/***
* @brief Callback event handler implemented by the application to receive
*   notification of state changes and signals enabled by the state definitions.
* @param id The callback event identifier.
* @param epoch The time of the event.
* @param ctMeas The number of measurements in the event.
*/
forward app_TrkCallbackEvent(  
	idTcbe,
    epoch,
    ctMeas
);

/***
* @brief Callback event handler implemented by the application to receive
*   notification of event state changes.
* @param idEvent The stateful event identifier.
* @param newState  The new state identifier.
*/
forward app_StatefulEventCallback(  idEvent, StatefulEventState: newState);
