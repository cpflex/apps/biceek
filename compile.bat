@ECHO OFF
if [%1]==[] goto usage

set TESTSYM=
set SYSLOG=
if [%2]==[TEST] (set TESTSYM=__TEST__=1)
if [%3]==[TEST] (set TESTSYM=__TEST__=1)
if [%2]==[SYSLOG] (set SYSLOG=__SYSLOG__=1)
if [%3]==[SYSLOG] (set SYSLOG=__SYSLOG__=1)

REM if [%2]  == [] (set TESTSYM=)
REM if [%3]  == [] (set SYSLOG=)

set lib=../../v1/lib

set _DATE=%date%
set _TIME=%time%
set _VERSION=%1

REM Display Header.
ECHO Compiling Biceek Application: 
ECHO Version: %_VERSION%
ECHO Date: %_DATE%
ECHO Time: %_TIME%
ECHO Mode:  %TESTSYM%
ECHO Syslog: %SYSLOG%
 

REM Create Version File with string data.
ECHO stock const __VERSION__{} = "%_VERSION%"; > src/version.i
ECHO stock const __DATE__{} = "%_DATE%"; >>		 src/version.i
ECHO stock const __TIME__{} = "%_TIME%"; >>		 src/version.i

pawncc ^
	%lib%/battery.p %lib%/SimpleTracking.p %lib%/NvmRecTools.p ^
	%lib%/TelemMgr.p %lib%/LocationConfig.p %lib%/SysMgr.p ^
	 %lib%/StatefulEvent.p Controller.p app.p  ^
	-Dsrc -S256 -X32768 -XD4092 ^
	-i. -i../../v1/include -i%lib% ^
	-o../biceek.bin ^
	%TESTSYM% %SYSLOG%
	
	

goto :eof
:usage
@echo Usage: %0 ^<version^> [TEST] [SYSLOG]
exit /B 1

