# Biceek Application Release Notes

### V1.008 - 231206
1. Updated v1 Library version to synchronize with other alpha builds

### V1.007 - 231128
1. Updated CT1XXX firmware reference to v2.4.4.2a
 - Fixed TimeSync bug (KP-350)

### V1.006 - 231117
1. Updated CT1XXX firmware reference to v2.4.4.1a  See release notes for more information

### V1.005 - 231116
1. Updated CT1XXX firmware reference to v2.4.4.0a  See release notes for more information
 - v1.04 LoRaWAN 
 - US915 SB2 / ADR Disabled config
 - EU868 standard band / ADR enabled config
2. Changed Active Motion sampling/thresholds from 1hz@100mG to 10hz@500mG 

### V1.004 - 231016
1. Bugfix: Fixed motion stopped (parked) behavior.

### V1.003a - 231013
1. Bugfix: reporting Enter motion repeatedly.   Now triggers only once when continuous motion begins.
2. Chore:  removed extraneous log messages.  Now only log when device enters/exits recovery mode.

### V1.002a - 231013
1. Updated CT1020 firmware reference to v2.3.0.1 since v2.3.0.0 was a bad build.
  
### V1.001a - 231011
1. bugfix appapi reference had a space in it.

### V1.000a - 231011
1. Added CT102X target support version v2.3.0.0
2. Flex Platform: v1.4.1.0
3. Standard EU band ADR enabled.
   
---
*Copyright 2023, Codepoint Technologies, Inc.* 
*All Rights Reserved*
