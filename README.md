# Biceek Bicycle Tracker Application
**Target: CT100X, CT102X Series Devices**<br/>
**Version: 1.008br/>**
**Release Date: 2023/12/06**

Application implements tracking functions needed for managing bicycles deployed throughout North America and Europe.
The purpose of the application is to keep track of the bikes and provide a recovery mode if lost
or stolen.  Implementation is designed to maximize battery life given cold weather.


## Key Features  

1. Approximately 9 month battery life between charges.
2. Two operating modes:normal and recovery.  Modes are managed by Traxmate/Biceek application.
3. Configurable recovery mode tracking interval (minimum 3 minutes)
4. Eight (8) location reports per day on average -- normal mode reports when parked (stopped) more than 10 minutes, every report is confirmed. 
5. Network check minimum 4 times a day, location report at least once per day.
6. Charging, discharging, and battery status (every 5%) reports.
7. Power saving mode at 10%, disables location reports unless explicitly requested.
8. Sending a "bike moving" message when continuous motion is detected for 15s+ after parking (stopping).
9. Performs a network check five (5) minutes after continuous motion is detected to check for network downlinks.
11. Cache messages when out of network except when in recovery mode.
12. Reports location daily if not active.

## Operating Mode Specifications
The Bike has two operating modes which governs behavior.   They are defined as follows.

* **Normal Mode** -- Normal operating mode reports location (confirmed) when a device has stopped (motionless) for more 5 minutes. 
  Assuming a bike makes eight (8) trips per day on average, there will be about 8 reports each day. The device 
  will perform a network check a minimum four (4) times per day. Every location report in normal mode is a confirmed/archived message. 
* **Recovery** -- The network can command the device to enter into a recovery mode, where the tag will report every three (the default) minutes
  when on network.  No locations are reported or cached when off network. Location archiving is disabled so only the current location
  is reported. Confirmation of messages follows standard confirmation pattern (every 3rd message).

## Remote Management Features
To assist with managing the devices in the field, these capabilities are provided. Traxmate/Biceek system 
administrators can access these functions.
1. Enable / Disable System Logging (disabled by default)
2. Erase Archive Cache
3. Biceek Protocol API V1.0.0.0 Support with supporting device data type (Biceek) in Traxmate (device type id= 142792). 

## Button Descriptions (CT1000 Only)

The following table summarizes the user interface for the application.  See the sections below for more detailed information regarding each button action.  

There are two user actions battery and network status that can be accessed
using either of the buttons.

| **Command**                         |     **Action**       |     **Button**     |  **Description**           |
|-------------------------------------|:--------------------:|:------------------:|---------------------------|
| **Battery Level**                   | One (1) quick press  | any button or both | Battery status (see Battery Indicator below).    |  
| **Network Coverage**                | Two (2) quick press  | any button or both | Network status (see Coverage Indicator below)    |  
| **Network Reset**                   | hold > 15<br>seconds | any button or both | Network reset device      |
| **Factory Reset**                   | hold > 25<br>seconds | any button or both | Resets the network and configuration to factory defaults for the application.     |

### Test Build Button Actions (CT1000 Only)
When the device is compiled in TEST mode, additional button actions are enabled to
accelerate testing.   They are shown in the table below.  

| **Command**                         |     **Action**        |     **Button**     |  **Description**           |
|-------------------------------------|:---------------------:|:------------------:|---------------------------|
| **Set Normal Mode**                 | Three (3) quick press | any button or both | Sets the device into normal operating state.    |  
| **Set Recovery Mode**               | Five (5) quick press  | any button or both | Sets the device into recovery operating state.    |  

## LED Descriptions

The following table indicates the meaning of the two bi-color LEDs on the device.  The LED's are
positioned:

* **LED #1** - Upper LED, furthest away from charger pads on CT1000.   Only LED on CT102X series.
* **LED #2** - Lower LED, close to charger pads on CT1000.   Not supported CT102X.

| Indication                 | LEDS     | Description |
|:---------------------------| :-------:|:----------------------------|
| One second orange blink    | LED #2   | Battery charging            |
| Continuous green           | LED #2   | Battery fully charged       |
| Green blink three times    | both     | Device reset                |

See status indicators below for additional LED signalling when requesting battery and network status.

### Battery Status Indicator 

Quickly pressing Button #2 will indicate the percent charge of the battery in 20% increments.

LED #2 will blink as follows to indicate percent charge.

| LED #2  | % Charge     | Description                                     |
|----------|-------------|-------------------------------------------------|
|  1 red   |   < 10%     | Battery level is critical,  charge immediately. |
|  1 green |  15% - 30%  | Battery low  less than 30% remaining.           |
|  2 green |  30% - 50%  | Battery is about 40% charged.                    |
|  3 green |  50% - 70%  | Battery is about 60% charged.                    |
|  4 green |  70% - 90%  | Battery  is about 80% charged.                   |
|  5 green |  > 90%      | Battery is fully charged.                       |

### Network Status Indicator 

Quickly pressing Button #2 twice will indicate the status of LoRaWAN network coverage.  

LED #1 will blink as follows to indicate coverage information

| LED #1   | Signal Strength (dBm) | Description                                |
|----------|-----------------------|--------------------------------------------|
|  4 green |  -64  to -30          | Very strong signal strength                |
|  3 green |  -89  to  -65         | Good signal strength                       |
|  2 green |  -109  to -90         | Low signal strength                        |
|  1 green |  -120 to -110         | Very low signal strength                   |
|  1 red   |                       | Network unavailable (out of range)         |
|  2 red   |                       | LoRaWAN telemetry disabled.                |


## Integration

### Traxmate integration
To work with the Traxmate application a custom device type is registered.  Set your device type to Cora Biceek Bike Tracker (id=142792).  Use this ID in the cloud stack device templates to pre-configure the device type.   

### Testing 
A postman collection is provided documenting all the downlink messages that can be sent to the Bike app. This can be found in the test folder.

---
*Copyright 2023, Codepoint Technologies, Inc.* 
*All Rights Reserved*
  
