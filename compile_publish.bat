@ECHO OFF
if [%1]==[] goto usage

echo ** Compiling /  Publishing Release version and marking as Latest.
call compile %1
call cphub_push %1
cphub update  /bobcat/apps/biceek:%1 -l

goto :eof
:usage
@echo Usage: %0 ^<tag (e.g. v1.0.2.3)^> 
exit /B 1
